terraform {
  cloud {
    organization = "ehlo"

    workspaces {
      name = "my-example"
    }
  }
}
