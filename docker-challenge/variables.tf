variable "container_name" {
  description = "name of the container"
  type        = string
  default     = "AltaResearchWebService"
}
variable "external_port" {
  description = "external port"
  type        = number
  default     = 5432
}
variable "internal_port" {
  description = "internal port"
  type        = number
  default     = 9876
}

